**Visualization tool for account activities**

<img src="images/screenshot.png" width="1500" height="500">



**Build docker image:**

docker build -t accsec/node .

**Start docker:** 

docker run -p 8080:8080 -d accsec/node
