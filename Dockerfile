FROM node:6

# Create app directory
WORKDIR /usr/src/app

# Bundle app source
COPY . .

EXPOSE 8080
#RUN npm init 
RUN npm install express --save

CMD ["node", "app.js" ]
